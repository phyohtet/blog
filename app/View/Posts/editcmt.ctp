<?php echo $this->Html->link('Post Details', array ('controller' => 'posts', 'action' => 'view', $comment['Comment']['foreign_id'])) . ">>Edit Comment"; ?>
<h2>Edit Comment</h2>
<?php
	echo $this->Form->create('Comment', array('enctype' => 'multipart/form-data'));
	echo $this->Form->input('body', array ("label" => "Update Body"), array('rows' => '3'));
	echo $this->Form->input('upload', array ('type' => 'file', "label" => "Update Comment Photo"));
	echo $this->Form->input('id', array ('type' => 'hidden'));
	echo $this->Form->end('Save Comment');
	// echo $this->Html->link('Back', array ('controller' => 'posts', 'action' => 'view',$comment['Comment']['foreign_id']));
?>