
<?php echo $this->Session->flash(); ?>
<div  id="cus">
    <table>
        <tr>
            <td>
                <h2>Blog posts</h2>
            </td>
            <td align="right">
                <?php echo $this->Html->link( "Logout",array('controller' => 'Users' ,
                                                        'action' => 'logout') ); ?>
            </td>
        </tr>
    </table>
    </div>
    <div id="search">
        <form id="postsform"  action="<?php echo $this->html->url('/posts/search'); ?>" method="post" enctype="multipart/form-data">
            <table id="cus">
                <tr>
                    <td>
                        <label for = "searchid"> Search ID: </label>
                        <input type="text" name="searchid"/>
                    </td>
                    <td>
                        <label for = "searchtitle"> Search Title: </label>
                        <input type="text" name="searchtitle"/>
                    </td>
                    <td>
                        <label for = "searchbody"> Search Body: </label>
                        <input type="text" name="searchbody"/>
                    </td>
                </tr>
            </table>
            <?php
                echo $this->Form->end('Search'); ;
            ?>
        </form>
</div>

<p> 
    <?php 
        $paginator = $this->Paginator;
        if ($posts) {
        echo $this->Html->link('ADD POST', array ('action' =>  'add'));
    ?>
</p>
    <table>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Actions</th>
            <th>Author</th>
            <th>Created</th>
        </tr>
        <?php foreach($posts as $post): ?>
        
        <tr>
            <td>
                <?php echo $post['Post']['id'];?>
            </td>
            <td>
                <?php echo $this->Html->link($post['Post']['title'],
                                            array('controller'=> 'posts',
                                            'action'  =>  'view',$post['Post']['id'])); ?>
            </td>
            <td>
                <?php 
                    $uid = $this->Session->read('Auth.User.id');
                    if($uid == $post['User']['id']) {
                        echo $this->Form->postLink('Delete', array ('action' => 'delete', $post['Post']['id']),
                            array ('confirm' => 'Are u sure you want to delete?'));

                        echo "|". $this->Html->link('Edit', array ('action' => 'edit', $post['Post']['id']));
                    }   
                ?>
            </td>
            <td> <?php echo $post['Post']['username']; ?> </td>
            <td>
                <?php echo $post['Post']['created']; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
    <?php
        echo "<div class='paging'>";
            // the 'first' page button
            echo $paginator->first("First");
            // 'prev' page button, 
            if ($paginator->hasPrev()) {
                echo $paginator->prev("Prev");
            }
            // the 'number' page buttons
            echo $paginator->numbers(array ('modulus' => 2));
            // for the 'next' button
            if ($paginator->hasNext()) {
                echo $paginator->next("Next");
            }
            // the 'last' page button
            echo $paginator->last("Last");
        echo "</div>";
    }
    else{
        echo "No users found.";
    }
    ?>

