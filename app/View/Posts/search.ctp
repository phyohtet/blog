<!-- start of search form -->
<div id="search">
	<form id="postsform" action="<?php echo $this->html->url('/posts/search'); ?>" method="post" 
		enctype="multipart/form-data">
	<table border="0">
        <tr>
            <td>
            	<label for = "searchid"> Search ID: </label>
            	<input type="text" name="searchid"/>
            </td>
            <td>
            	<label for = "searchtitle"> Search Title: </label>
            	<input type="text" name="searchtitle"/>
            </td>
            <td>
            	<label for = "searchbody"> Search Body: </label>
            	<input type="text" name="searchbody"/>
            </td>
        </tr>
    </table>
	<?php echo $this->form->end('Search'); ?>
</div>
<?php 
	if (empty($results)) {
		echo "Not Found. Search Again!!!";

	}
	else {
?>
<div id="contentbox">
	<h2>Manage Post</h2>
	<table>
		<thead>
			<th> ID </th>
			<th> Title </th>
			<th> Body </th>
			<th> Created Date </th>
		</thead>
		<?php foreach($results as $posts) : ?>  
			<tr>
				<td><?php echo $posts['Post']['id'] ?></td>
				<td><?php echo $posts['Post']['title'] ?></td>
				<td><?php echo $posts['Post']['body'] ?></td>
				<td><?php echo $posts['Post']['created'] ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->Html->link('Back',array('action' => 'index')); ?>
</div>
<?php } ?>