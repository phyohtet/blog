
<?php echo $this->Html->link('Home', array ('action' => 'index')). ">>Post Details"; ?>
<h2> <?php echo h($post['Post']['title']); ?> </h2>

<?php echo "By ". $post['Post']['username']; ?>
<p> 
	<small> Created: <?php echo $post['Post']['created']; ?> </small>
</p>
<p>
	<?php echo h($post['Post']['body']); ?> 
</p>
<center>
	<?php 
		if(!empty($post['Post']['image'])) {
		echo $this->Html->image($post['Post']['image'], 
			array ('alt' => '', 'border' => '1', 'width' => '600', 'height' => '500'));
		}
	?>
</center>

<?php if ( ! empty($post['Comment'])): ?>
	<?php foreach ($post['Comment'] as $comment): ?>
    	<br><?php echo "<b>" . $comment['name'] .  "</b>"; ?><br>
    	<?php 
		if(!empty($comment['image'])) {
			echo $this->Html->image($comment['image'], 
				array ('alt' => '', 'border' => '1', 'width' => '100', 'height' => '100'));
		}
		?> 
    	<?php echo $comment['body']; ?>
    	<?php
   			$username = $this->Session->read('Auth.User.username');
    		if($username == $comment['name']){
		        echo "... ".$this->Form->PostLink('Delete',
		        array ('controller' => 'posts', 'action'=>'deletecmt',$comment['id'],$comment['foreign_id']),
		        array ('confirm'=>'Are u sure'));
		        echo "|". $this->Form->PostLink('Edit',
		        array ('controller' => 'posts', 'action'=>'editcmt',$comment['id'],$comment['foreign_id'])) . "<br>";
    		}
    	?>
   	<br>
	<?php endforeach; ?>
<?php else: ?>
	<p>No comments...</p>
<?php endif; ?>

<?php echo $this->Form->create('Comment', array ('enctype' => 'multipart/form-data', 'action' => array ('controller' => 'posts', 'action' => 'addcmt', $post['Post']['id']))); ?>
<?php echo $this->Form->input('Comment.body', array ("label" => "", "placeholder" => "Write your comment...", "rows" => "2")); ?>
<?php echo $this->Form->input('Comment.upload', array ('type' => 'file', "label" => "Comment Photo")); ?>
<?php 
	echo $this->Form->end('Submit');
	// echo $this->Html->link('Back', array ('action' => 'index'));
?>


