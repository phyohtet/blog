<?php echo $this->Html->link('Home', array ('action' => 'index')) . ">>Edit Post"; ?>
<h2>Edit Post</h2>
<?php
	echo $this->Form->create('Post', array ('enctype' => 'multipart/form-data'));
	echo $this->Form->input('title');
	echo $this->Form->input('upload', array('type' => 'file', 'method' => 'post', 'enctype' => 'multipart/form-data'));
	echo $this->Form->input('body', array ('rows' => '7'));
	echo $this->Form->input('id', array ('type' => 'hidden'));
	echo $this->Form->end('Save Post');
	// echo $this->Html->link('Back', array ('action' => 'index'));
?>


