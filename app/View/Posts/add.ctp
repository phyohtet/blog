<?php echo $this->Html->link('Home', array ('action' => 'index')) . ">>Add Post"; ?>
<h2>Add Post</h2>
<?php
	echo $this->Form->create('Post', array('enctype' => 'multipart/form-data'));
	echo $this->Form->input('title');
	echo $this->Form->input('upload', array('type'=>'file'));
	echo $this->Form->input('body', array ('rows' => '3'));
	echo $this->Form->end('Save Post');
	// echo $this->Html->link('Back', array ('action' => 'index'));
?>
