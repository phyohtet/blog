<div>
    <p>
        <?php echo $this->Html->link('Back to Login', array ('action' => 'login')); ?>
    </p>
</div>
<div class = "users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend>
            <?php echo __('Register User...'); ?>
        </legend>
        <?php 
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('confirm_password', 
                                    array('type'=>'password', 'value'=>'', 'autocomplete'=>'off'));
            echo $this->Form->input('role', array('options' => array('admin' => 'Admin', 
                                                'author' => 'Author')));
        ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>