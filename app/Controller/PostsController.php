<?php
class PostsController extends AppController {
	public $helpers = array('Html', 'Form');
	public $component = array('Paginator');
    var $name = 'Posts';
	function search() {
		$searchid = $_POST['searchid'];
		$searchtitle = $_POST['searchtitle'];
		$searchbody = $_POST['searchbody'];
		if($searchbody == "" && $searchid == "" && $searchtitle == "") {
            echo $this->Session->setFlash('Enter something a texfield...', 'default', array('class' => 'error-message'));
			$this->redirect(array('action' => 'index'));
		}
		else {
    		if($searchid != "") { 
                $field = "Post.id"; 
                $value = $searchid;
            }
    		if($searchtitle != "") {
                $field = "title"; 
                $value = $searchtitle;
            }
    		if($searchbody != "") {
                $field = "body"; 
                $value = $searchbody;
            }
		}
    	$results = $this->Post->find('all', array('fields' => array('Post.id', 'Post.title', 'Post.body', 'Post.created', 'Post.modified'), 'posts' => 'Post.id ASC',
    											                    'conditions' =>array($field.' '.'LIKE' => '%'.$value.'%')));
    	$this->set('results', $results);
    }
	public function index() {
        $this->set('posts', $this->Post->find('all'));
        $this->paginate = array ('limit' => 5, 'order' => array ('posts.id' => 'asc'));
        $posts = $this->paginate('Post');
        $this->set('posts', $posts);
    }
    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
            // $this->_flash(__('Invalid Post.', true),'error');
            $this->redirect(array('action' => 'index'));
        }
        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('post', $post);
    }
	public function add() {
        if ($this->request->is('post')) {
            $this->Post->create();
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            $this->request->data['Post']['username'] = $this->Auth->user('username');
            if(!empty($this->request->data)) {
                if(!empty($this->request->data['Post']['upload']['name'])) {
                    $file = $this->request->data['Post']['upload'];
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png');
                    if(in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
                        $this->request->data['Post']['image'] = "/img/". $file['name'];
                        // pr($this->request->data);
                    }
                }
            }
            if ($this->Post->save($this->request->data)) {
                echo $this->Session->setFlash('Your post has been saved', 'default', array('class' => 'success'));
                return $this->redirect(array('action' => 'index'));
            }
            echo $this->Session->setFlash('Unable to add your post.', 'default', array('class' => 'error-message'));
        }
    }
    public function addcmt($id = null){
        // save the comment
        if($this->request->is('post')) {
            if (!empty($this->data['Comment'])) {
                $this->request->data['Comment']['class'] = 'Post'; 
                $this->request->data['Comment']['foreign_id'] = $id;
                $this->request->data['Comment']['name'] = $this->Auth->user('username');
                $this->Post->Comment->create(); 
                if(!empty($this->request->data)) {
                    if(!empty($this->request->data['Comment']['upload']['name'])) {
                        $file = $this->request->data['Comment']['upload'];
                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                        $arr_ext = array('jpg', 'jpeg', 'gif', 'png');
                        if(in_array($ext, $arr_ext)) {
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
                            $this->request->data['Comment']['image'] = "/img/". $file['name'];
                        }
                    }
                }
                // pr($this->request->data);
                if ($this->Post->Comment->save($this->data)) {
                    echo $this->Session->setFlash('The Comment has been saved.', 'default', array('class' => 'success'));
                    $this->redirect(array ('action' => 'view', $id));
                }
                echo $this->Session->setFlash('The Comment could not be saved. Please, try again.', 'default', array('class' => 'error-message'));
            }
            // set the view variables
            $post = $this->Post->read(null, $id);
            $this->set(compact('post'));
        }
    }
    public function edit($id = null) {
    	if (!$id) {
        	throw new NotFoundException(__('Invalid post'));
    	}
        if(!empty($this->request->data)) {
            if(!empty($this->request->data['Post']['upload']['name'])) {
                $file = $this->request->data['Post']['upload'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png');
                if(in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
                    $this->request->data['Post']['image'] = "/img/". $file['name'];
                    // pr($this->request->data);
                }
            }
        }
    	$post = $this->Post->findById($id);
    	if (!$post) {
        	throw new NotFoundException(__('Invalid post'));
    	}
    	if ($this->request->is(array('post', 'put'))) {
        	$this->Post->id = $id;
              	if ($this->Post->save($this->request->data)) {
                echo $this->Session->setFlash('Contact added successfully!', 'default', array('class' => 'success'));
            	return $this->redirect(array('action' => 'index'));
        	}
            echo $this->Session->setFlash('Unable to update your post.', 'default', array('class' => 'error-message'));
    	}
    	if (!$this->request->data) {
        	$this->request->data = $post;
    	}
	}
    public function editcmt($id = null, $foreign_id) {
        if (!$id) {
                throw new NotFoundException(__('Invalid post'));
            }
            if(!empty($this->request->data)) {
                if(!empty($this->request->data['Comment']['upload']['name'])) {
                    $file = $this->request->data['Comment']['upload'];
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png');
                    if(in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
                        $this->request->data['Comment']['image'] = "/img/". $file['name'];
                    }
                }
            }
            $comment = $this->Post->Comment->findById($id);
            if (!$comment) {
                throw new NotFoundException(__('Invalid post'));
            }
            if ($this->request->is(array('post', 'put'))) {
                // $this->request->data['Comment']['foreign_id'] = $fid;
                // $this->Post->Comment->foreign_id = $foreign_id;
                $this->Post->Comment->id = $id;
                if ($this->Post->Comment->save($this->request->data)) {
                    echo $this->Session->setFlash('Contact added successfully!', 'default', array('class' => 'success'));
                    return $this->redirect(array('action' => 'view',$foreign_id));
                    // $this->redirect(array ('action' => 'view', $id));
                }
                // $this->Session->setFlash('Unable to update your post.');
            }
            if (!$this->request->data) {
                $this->request->data = $comment;
            }
            $this->set('comment', $comment);
    }
	public function delete($id = null) {
    	if ($this->request->is('get')) {
        	throw new MethodNotAllowedException();
    	}
    	if ($this->Post->delete($id)) {
            echo $this->Session->setFlash('The post has been deleted.', 'default', array('class' => 'success'));
    	} else {
            echo $this->Session->setFlash('The post could not be deleted.', 'default', array('class' => 'error-message'));
    	}
		return $this->redirect(array('action' => 'index'));
	}
    public function deletecmt($id = null, $foreign_id) {
        if (!$id) {  
            $this->Session->setFlash('Invalid id for comment');  
            $this->redirect(array('action' => 'index'));  
        }
        if ($this->Post->Comment->delete($id)) { 
             // $this->Session->setFlash('Comment deleted');
            echo $this->Session->setFlash("Comment has been deleted", 'default', array('class' => 'success'));
            $this->redirect(array('action' => 'view', $foreign_id));
             
  
        } else {  
            echo $this->Session->setFlash("Comment was not deleted", 'default', array('class' => 'error_message'));
        }
    }
    public function isAuthorized($user) {
        // All registered users can add posts
        if ($this->action === 'add') {
            return true;
        }
        // The owner of a post can edit and delete it
        if (in_array($this->action, array('edit', 'delete'))) {
            $postId = (int) $this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $user['id'])) {
                return true;
            }
        }

        return parent::isAuthorized($user);
    }
	
}
